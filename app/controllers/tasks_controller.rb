class TasksController < ApplicationController
  before_action :ensure_json_request
  before_action :set_task, only: [:show, :update, :destroy]

  def show
    render status: :ok, json: {  
      message: 'Loaded task', 
      data: @task 
    }
  end

  def index
    @incomplete_tasks = Task.all_incomplete.ordered
    @completed_tasks = Task.all_completed.ordered
    @archived_tasks = Task.all_archived.ordered

    render status: :ok, json: {
      message: 'Loaded all tasks',
      data: {
        incomplete_tasks: @incomplete_tasks,
        completed_tasks: @completed_tasks,
        archived_tasks: @archived_tasks
      }
    }
  end

  def create
    @task = Task.new(task_params)
    
    if @task.save
      render status: :created, json: { 
        message: 'Created task successfully.', 
        data: @task 
      }
    else
      render status: :unprocessable_entity, json: @task.errors 
    end
  end

  def update
    if @task.update(task_params)
      render status: :ok, json: { 
        message: 'Updated task successfully.', 
          data: @task 
      }
    else
      render status: :unprocessable_entity, json: @task.errors 
    end
  end

  def destroy
    @task.destroy
      render status: :no_content, json: { 
      message: 'Task destroyed successfully'
    }
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:title, :note, :completed)
    end

    def ensure_json_request
      return if request.format == :json
      render nothing: :true, status: :not_acceptable # HTTP 406 :: Not Acceptable
    end
end
