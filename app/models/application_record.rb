class ApplicationRecord < ActiveRecord::Base

  def self.has_statuses( *status_names )
    validates :status,
      presence: true,
      inclusion: { in: status_names.map { |name| name.to_s } }

    # Status Finders
    status_names.each do |status_name|
      scope "all_#{status_name}", -> {
        where( status: status_name.to_s )
      }
    end

    # Status Accessors
    status_names.each do |status_name|
      define_method "#{status_name}?" do
        status == status_name.to_s
      end
    end
  end

  self.abstract_class = true

end
