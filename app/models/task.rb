class Task < ApplicationRecord
  has_statuses :incomplete, :completed, :archived
  validates :title, length: { minimum: 4 }
  validates :title, presence: true
  validates :note,  length: { maximum: 300 }

  scope :ordered, -> { order("created_at ASC") }
end
