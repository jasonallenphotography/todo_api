# README

A toy API project to prepare for the stack I will soon use at Full Measure. Steps taken:

* Set RBEnv (I use RBEnv instead of RVM in my current environment presently) to 2.3.1

* Installed MySQL:  
   ```:> brew install mysql```

* Installed Rails 5.1.4:  
   ```:> gem install rails 5.1.4```

* Initialized a new rails API app with the following CLI command:  
   ```:> rails new todo_api --database=mysql --api --skip-test --skip-turbolinks --skip-coffee```
  * Setting the database to MySql
  * Setting the ```rails new``` command to generate only API resources
  * Skipping minitest
  * Skipping TurboLinks Gem
  * Skipping Coffee Script Gem

* Initialized Git repo

* Included RSpec-rails gem and rebundled

* Create Task model, spec, migration
  * Add ```null: false``` constraint to ```CreateTasks``` migration
  * Add validations to ```task.rb```