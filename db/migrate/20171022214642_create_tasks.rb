class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :title, null: false
      t.string :status, null: false
      t.text :note, null: false
      t.date :completed_date

      t.timestamps
    end
  end
end
